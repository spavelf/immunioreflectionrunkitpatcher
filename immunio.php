<?php

namespace immunio;

use \ReflectionClass;
use \ReflectionMethod;

/**
 * Class Wrapper
 * @package immunio
 */
class Wrapper
{
  /**
   * @var ReflectionRunkitPatcher
   */
  private $patcher;

  /**
   * @param ReflectionRunkitPatcher $patcher
   */
  public function __construct(ReflectionRunkitPatcher $patcher) {
    $this->patcher = $patcher;
  }

  /**
   * Prepends the methods of the class initialized inside the patcher with the body of the code function
   */
  public function prependMethods() {
    $code_to_prepend =  $this->getCodeToPrepend();

    $this->patcher->prependMethods($code_to_prepend);
  }

  /**
   * Gets the body of the code function
   * @return mixed
   */
  private function getCodeToPrepend()
  {
    $wrapper_patcher = new ReflectionRunkitPatcher(new ReflectionClass(get_class($this)));
    return $wrapper_patcher->getMethodBodyByMethodName("code");
  }

  /**
   *
   */
  private function code() {
    $immunio_file = fopen("/tmp/immunio.txt", "a");
    $content =  "%s %s::%s()\n";
    fwrite($immunio_file, $content);
    fclose($immunio_file);
  }
}

/**
 * Class ReflectionRunkitPatcher
 * @package immunio
 */
class ReflectionRunkitPatcher
{
  /**
   * @var ReflectionClass
   */
  private $reflection_class;

  /**
   * @param ReflectionClass $reflection_class
   */
  public function __construct(ReflectionClass $reflection_class) {
        $this->reflection_class = $reflection_class;
    }

    /**
     * @param $code - the code to prepend to all the class methods.
     * The 3 occurrences of %s will be replaced by the timestamp, class name and method name respectively
     */
    public function prependMethods($code) {
        $class_name = $this->reflection_class->getName();
        $methods = $this->reflection_class->getMethods();
        foreach ($methods as $method) {
            $method_name = $method->getName();
            $method_body = $this->getMethodBody($method);
            $method_parameters = $this->getMethodParameters($method);
            $date = date("Y-m-d H:i:s");

            $code_to_prepend = sprintf($code, $date, $class_name, $method_name);
            $method_body = $code_to_prepend . $method_body;

            runkit_method_redefine(
                $class_name,
                $method_name,
                $method_parameters,
                $method_body,
                RUNKIT_ACC_PUBLIC
            );
        }
    }

  /**
   * Gets body of the method by method name
   * @param $method_name
   * @return string
   */
  public function getMethodBodyByMethodName($method_name)
  {
    $method = $this->reflection_class->getMethod("code");
    return $this->getMethodBody($method);
  }

  /**
   * Gets body of the method
   * @param ReflectionMethod $method
   * @return string
   */
  private function getMethodBody(ReflectionMethod $method)
  {
    $source = $this->getMethodSource($method);
    $patterns = array();
    $patterns[0] = '/^(.+?)\{/s';
    $patterns[1] = '/\}$/s';
    $replacements = array();
    $replacements[0] = '';
    $replacements[1] = '';

    return preg_replace($patterns, $replacements, $source);
  }

  /**
   * Gets source of the method (including function name and parenthesis)
   * @param ReflectionMethod $method
   * @return string
   */
  private function getMethodSource(ReflectionMethod $method)
    {
        $path = $method->getFileName();
        $lines = @file($path);
        $from = $method->getStartLine();
        $to = $method->getEndLine();
        $len = $to - $from;
        return trim(implode(array_slice($lines, $from - 1, $len + 1)));
    }

  /**
   * Get parameter list of the method delimited by comma
   * @param ReflectionMethod $method
   * @return string
   */
  private function getMethodParameters(ReflectionMethod $method) {
        $parameters_names = array();
        foreach($method->getParameters() as $parameter) {
            $parameters_names[] = "$" . $parameter->getName();
        }
        return implode(",", $parameters_names);
    }
}

require_once "immunio_config.php";

if(!class_exists($class_to_patch_name)) {
  require_once $class_to_patch_path;
}

$reflection_class = new ReflectionClass($class_to_patch_name);
$reflection_patcher = new ReflectionRunkitPatcher($reflection_class);
$wrapper = new Wrapper($reflection_patcher);
$wrapper->prependMethods();
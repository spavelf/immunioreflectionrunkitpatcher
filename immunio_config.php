<?php

namespace immunio;

//Config parameters

//Name of the class to patch
$class_to_patch_name = 'model';

//Optional - path to target class for patching. Optional and not needed in case the class is already loaded
$class_to_patch_path = 'tests/Model.php';
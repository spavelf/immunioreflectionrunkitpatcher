**ReflectionRunkitPatcher**
===================

The reflection runkit patcher is a script for modifying and prepend methods of the class of PHP application with custom code, without modifying the application source code.

The script uses reflection for converting the class methods code to string and runkit for redefining the class methods from the string consisting of the configurable code inside the scriot and original methods code.

----------

**Installation**
-------------

Simply paste the file inside the folder and include the file in your application either by composer or by require_once function of the PHP

> **Note:**

> - The only requirements for this to work is having the target class loaded before including the reflection runkit patcher script

Example of adding the script using the composer:

    {
	    "autoload": {
	        "files": ["immunio.php", "immunio_config.php" ]
	    }
	}

----------

**Configuration**
----------------

The script comes with the configuration file.

The configuration file have 2 variables:

class_to_patch_name - Name of the class to patch
class_to_patch_path - Optional - path to target class for patching. Optional and not needed in case the class is already loaded

----------

**Usage**
-------

The code you would like to prepend to each method of your class located in the Wrapper class inside the function named code() and can be replaced with any other code based on your needs

> **Note:** The default implementation includes adding the code that will write to some file the following:

> ***date, class_name, method_name***

> These values are replaces by the sprintf function during the reflection processing and could be omitted without affecting the patching functionality.
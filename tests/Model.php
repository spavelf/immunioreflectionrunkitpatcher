<?php

// Model.php
class Model {

    public function __construct() {
        echo "Constructor!\n";
    }

    function A($param_a)  {

        echo "Function A!\n";

        if ($i=6) {
            $r =5;
        }

        $this->C();
    }

    function B()  {
        echo "Function B!\n";

        if ($i=6) {
            $r =5;
        }
    }

    private function C() {
        echo "Private function C!\n";
    }
}
